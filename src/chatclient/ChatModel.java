/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author Com01
 */
public class ChatModel extends AbstractListModel<ChatDetail>{

    private ArrayList<ChatDetail> model;
    public ChatModel(){
        model = new ArrayList<ChatDetail>();
    }
    
    public ArrayList<ChatDetail> getChatArray(){
        return model;
    }
    
    public ChatModel(ArrayList<ChatDetail> model){
        this.model = model;
    }
    
    @Override
    public int getSize() {
        return model.size();
    }

    @Override
    public ChatDetail getElementAt(int index) {
        return model.get(index);
    }
   
}
